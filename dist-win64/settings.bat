@echo off

:: 工具配置参数（可修改）

:: 必配
set jdbc_link=jdbc:sqlserver://172.16.11.72:1433;databasename=crawler;user=admin;password=password

:: 两次连续爬取至少间隔时间，单位秒
set repeat_interval=10800

:: 起止日期过滤间隔，单位天
set history_days_bj=10
set history_days_hp=30
set history_days_zb=10

:: 如果网速过慢，或机器配置较低，适当调大以下参数，单位毫秒
set click_snap=2000
set nextpage_snap=2000
set switch_timeout=5000

:: 驱动
set browser=firefox

:: set browser=htmlunit
:: set selenium_server=127.0.0.1

:: set browser=phantomjs
:: set phantomjs_driver=phantomjs.exe

:: 高阶参数
set refresh_start=false
set driver_timeout=180000

set retries=5
set retry_snap=3000
set retry_snap_step=1000
set retry_snap_max=10000
set debug=false

:: 配置参数结束

set options=-Djava.net.preferIPv4Stack=true -Dzbcrawler.debug=%debug% -Dzbcrawler.repeat.period=%repeat_interval% -Dzbcrawler.switch.timeout=%switch_timeout% -Dzbcrawler.refresh.start=%refresh_start% -Dzbcrawler.driver.timeout=%driver_timeout% -Dzbcrawler.browser=%browser% -Dzbcrawler.retries=%retries% -Dzbcrawler.retry.snap=%retry_snap% -Dzbcrawler.retry.snap.step=%retry_snap_step% -Dzbcrawler.retry.snap.max=%retry_snap_max% -Dzbcrawler.click.snap=%click_snap% -Dzbcrawler.nextpageD.snap=%nextpage_snap% -Dzbcrawler.jdbc.link=%jdbc_link% -Dzbcrawler.history.days.bj=%history_days_bj% -Dzbcrawler.history.days.hp=%history_days_hp% -Dzbcrawler.history.days.zb=%history_days_zb% -Dzbcrawler.selenium.server=%selenium_server% -Dzbcrawler.phantomjs.executable=%phantomjs_driver%
