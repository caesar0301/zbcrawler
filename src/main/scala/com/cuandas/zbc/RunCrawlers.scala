package com.cuandas.zbc

import com.typesafe.scalalogging.LazyLogging
import com.cuandas.zbc.Utils._
import com.cuandas.zbc.crawlers.{BJCrawler, HPCrawler, ZBCrawler}

/**
  * Created by chenxm on 17-3-28.
  */
object RunCrawlers extends LazyLogging {

  private var startTime = -1L

  def main(args: Array[String]): Unit = {

    while(true) {
      startTime = currentSecs()

      logger.info("Running HP crawler ...")
      HPCrawler.main(args)

      logger.info("Running BJ crawler ...")
      BJCrawler.main(args)

      logger.info("Running ZB crawler ...")
      ZBCrawler.main(args)

      val s = REPEAT_PERIOD - secondsInterval()
      if (s > 0) {
        logger.info(s"Sleeping for ${s} seconds until next parsing ...")
        sleep(s * 1000)
      }

      clearSuccessStatusTables(List("crawler.bj_success", "crawler.zb_success"))
    }
  }

  private def secondsInterval(): Long = {
    currentSecs() - startTime
  }

  private def currentSecs(): Long = {
    (System.currentTimeMillis() / 1000).toInt
  }
}
