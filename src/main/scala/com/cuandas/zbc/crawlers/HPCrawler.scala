package com.cuandas.zbc.crawlers

import java.io.PrintWriter
import java.sql.DriverManager
import java.text.{ParseException, SimpleDateFormat}
import java.util.HashMap

import com.cuandas.zbc.Utils._
import org.openqa.selenium.{By, WebElement}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

private case class ProjectInfo
(
  projectName: String,
  constructionLocation: String,
  constructor: String,
  ecoEvalDepart: String,
  confirmFileName: String,
  confirmFileNo: String,
  confirmTime: String,
  confirmFileLink: String,
  postTime: String
) {
  def format(): String = {
    "('%s','%s','%s','%s','%s','%s','%s','%s','%s')".format(
      projectName,
      constructionLocation,
      constructor,
      ecoEvalDepart,
      confirmFileName,
      confirmFileNo,
      confirmTime,
      confirmFileLink,
      postTime
    )
  }
}

/**
  * 市场项目信息-环评阶段
  */
object HPCrawler extends WebCrawler {

  private val HOME_SITE = "http://www.sepb.gov.cn/hb/fa/cms/shhj/hpgs_list_login.jsp?channelId=2071&applyItem=1&gongshiType=3&approvType=1"
  private val CONTENT_FRAME = "/fa/cms/shhj/hpgs_gz_login.jsp?applyItem=1&gongshiType=3&approvType=1"
  private val outputFile = if (USE_FILE_BACKEND){
    makeUnifiedOutput("hp.txt")
  } else {
    null
  }
  private var validConstructors = List.empty[String]
  @volatile private var morePageExists = true

  def main(args: Array[String]): Unit = {

    validateDatabaseContext()

    // Read jsdw to filter records
    val con = DriverManager.getConnection(JDBC_LINK)
    val stmt = con.createStatement()
    val rs = stmt.executeQuery(s"select * from ${TB_JSDW}")
    val names = new mutable.ListBuffer[String]
    while(rs.next()) {
      names.append(rs.getString("name"))
    }
    validConstructors = names.toList
    rs.close()
    stmt.close()
    con.close()

    // Start parsing
    clearRunningContext
    start(List(""))
  }

  override def searchInternal(key: String) = {
    val projects: ListBuffer[ProjectInfo] = new ListBuffer[ProjectInfo]
    driver.navigate().to(HOME_SITE)
    // driver.get(HOME_SITE)

    // Switch to the child frame
    val iframes = driver.findElementsByXPath(s"//iframe[@src='${CONTENT_FRAME}']")
    switchToFrameWithTimeout(driver, iframes.get(0))

    // Check iagree box
    val checkbox = driver.findElementById("iAgree")
    if (!checkbox.isSelected)
      checkbox.click()

    // Click confirm
    driver.findElementByXPath("//input[@value='确认']").click()

    // History of parsed pages
    val parsedPages: HashMap[Int, String] = new HashMap[Int, String]()

    while (morePageExists) {
      // Extract all pages on current view
      var pagination: List[Tuple2[Int, WebElement]] = List.empty
      withSnapRetries(2000) { () =>
        pagination = driver.findElementsByClassName("numa")
          .map { e =>
            driver.executeScript("arguments[0].scrollIntoView(true);", e)
            Tuple2(e.getText.toInt, e)
          }
          .sortBy(_._1).toList
      }

      // Find the smallest unparsed page
      var pageIndex: Int = -1
      var page: WebElement = null
      pagination.foreach { case (index, p) =>
        if (pageIndex < 0 && !parsedPages.containsKey(index)) {
          pageIndex = index
          page = p
        }
      }

      // Extract entries in table
      if (pageIndex > 0) {
        logger.info(s"Parsing page [${pageIndex}]")
        page.click()

        // Behave elegantly to server and waiting for page refresh
        withSnapRetries(1000) { () =>
          extractTableContent(driver
            .findElementByXPath("//form[@name='condForm']//table[@class='tab']")
          ).foreach { p =>
            projects.append(p)
          }
        }

        // Save data
        if (projects.nonEmpty) {
          dumpProjects(projects)
          projects.clear()
        }
        parsedPages.put(pageIndex, "")

        if (isDebugging) {
          morePageExists = false
        }
      } else {
        morePageExists = false
      }
    }

    // Save data finally
    if (projects.nonEmpty) {
      dumpProjects(projects)
    }
  }

  private def extractTableContent(table: WebElement): Array[ProjectInfo] = {
    val rows = table.findElements(By.xpath("tbody/tr"))

    // Filter table entries via temporal range
    val (startDate, endDate) = historyInterval(HISTORY_DAYS_HP)
    val sdf = new SimpleDateFormat("yyyy-MM-dd")
    val startDateTime = sdf.parse(startDate)

    rows.map { row =>
      val columns = try {
        row.findElements(By.xpath("td")).toList
      } catch {
        case e: Exception =>
          logger.error(e.getMessage, e.getCause)
          List.empty[WebElement]
      }

      if (columns.nonEmpty) {
        if (isDebugging) {
          assert(columns.length == 9)
        }

        def robustColumn(index: Int): String = {
          try {
            columns.get(index).getText
          } catch {
            case e: Exception => ""
          }
        }

        val projectName = robustColumn(0)
        val constructionLocation = robustColumn(1)
        val constructor = robustColumn(2)
        val ecoEvalDepart = robustColumn(3)
        val confirmFileName = robustColumn(4)
        val confirmFileNo = robustColumn(5)
        val confirmTime = robustColumn(6)
        val confirmFileLink = robustColumn(7)
        val postTime = robustColumn(8).replaceAll("[\\r\\n]+", "")

        try {
          val ctime = sdf.parse(confirmTime.toString)
          if (ctime != null && ctime.getTime < startDateTime.getTime) {
            // Stop parsing when entries lie outside temporal range
            morePageExists = false
          }
        } catch {
          case e: ParseException =>
        }

        ProjectInfo(
          projectName = projectName,
          constructionLocation = constructionLocation,
          constructor = constructor,
          ecoEvalDepart = ecoEvalDepart,
          confirmFileName = confirmFileName,
          confirmFileNo = confirmFileNo,
          confirmTime = confirmTime,
          confirmFileLink = "",
          postTime = postTime
        )
      } else {
        null
      }
    }.toArray.filter(_ != null)
  }

  private def dumpProjects(projects: ListBuffer[ProjectInfo]): Unit = {
    if (USE_FILE_BACKEND) {
      val writer = new PrintWriter(outputFile, "utf-8")
      projects.foreach { p =>
        writer.println(s"${p.projectName}|${p.constructionLocation}|" +
          s"${p.constructor}|${p.ecoEvalDepart}|${p.confirmFileName}|${p.confirmFileNo}|" +
          s"${p.confirmTime}|${p.confirmFileLink}|${p.postTime}")
      }
      writer.close()
    } else {
      val con = DriverManager.getConnection(JDBC_LINK)
      val stmt = con.createStatement()
      val projectsFiltered = if (FILTER_HP_CONSTRUCTORS) {
        projects.filter { p =>
          val constructor = p.constructor
          constructor.isEmpty ||
            validConstructors.contains(constructor) ||
            validConstructors.exists(c => constructor.contains(c))
        }
      } else {
        projects
      }

      projectsFiltered.foreach { p =>
        val cmd = if (isMysqlBackend) {
          s"INSERT INTO ${TB_HP} VALUES ${p.format()} ON DUPLICATE KEY UPDATE postTime='${p.postTime}'"
        } else {
          s"INSERT INTO ${TB_HP} VALUES ${p.format()}"
        }
        executeUpsertSilently(stmt, cmd)
      }
      stmt.close()
      con.close()
    }
  }
}
