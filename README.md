ZBC
===

Development
------------

* sbt 0.13.6+
* Java 1.8+
* Python 3.5+
* GeckoDriver 0.14
* Firefox 48+

Deploy
------

* Supported browsers: Firefox, InternetExplorer, Edge
* Download Driver: https://seleniumhq.github.io/docs/wd.html

Usage
------

Build the package
```bash
sbt assembly
```

Run testing utility
```bash
./bin/runJob.sh -Dzbcrawler.jdbc.link=<jdbc> com.cuandas.zbc.[HP|BJ|ZB]Crawler
```
