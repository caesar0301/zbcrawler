
name := "zb-crawler"

version := "1.0"

scalaVersion := "2.11.8"

organization := "com.cuandas"

libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "3.2.0"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"

libraryDependencies += "log4j" % "log4j" % "1.2.17" % "test"

libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.22"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

libraryDependencies += "org.jsoup" % "jsoup" % "1.10.2"

libraryDependencies += "org.apache.commons" % "commons-io" % "1.3.2"

libraryDependencies += "mysql" % "mysql-connector-java" % "6.0.6"

// libraryDependencies += "net.sourceforge.jtds" % "jtds" % "1.3.1"

libraryDependencies += "com.microsoft.sqlserver" % "mssql-jdbc" % "6.1.0.jre8"

// libraryDependencies += "net.sourceforge.htmlunit" % "htmlunit" % "2.26"

libraryDependencies += "org.jprocesses" % "jProcesses" % "1.6.3"

assemblyMergeStrategy in assembly := {
  case PathList("javax", "xml", xs @ _*)         => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
