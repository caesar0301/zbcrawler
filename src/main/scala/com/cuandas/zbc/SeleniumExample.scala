package com.cuandas.zbc

import java.net.URL
import java.util.concurrent.TimeUnit

import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.{DesiredCapabilities, RemoteWebDriver}

/**
  * Created by chenxm on 17-3-11.
  */
object SeleniumExample {

  def main(args: Array[String]): Unit = {
    val capabilities = DesiredCapabilities.htmlUnitWithJs()
    capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true)
    capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true)
    capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, false)
    val driver = new RemoteWebDriver(new URL("http://172.16.11.72:4444/wd/hub"), capabilities)
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS)
    driver.get("http://www.baidu.com")
    // driver.close()
    driver.quit()
  }
}
