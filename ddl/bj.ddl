create database if not exists crawler DEFAULT CHARSET utf8;

create table if not exists crawler.bj (
searchKey VARCHAR(512),
subNumber VARCHAR(512),
projectName VARCHAR(512),
subTime VARCHAR(512),
subCategory VARCHAR(512),
permission VARCHAR(512),
const VARCHAR(512),
constLoc VARCHAR(512),
investment VARCHAR(512),
constScale VARCHAR(512),
agency VARCHAR(512),
url VARCHAR(512)
);

create table if not exists crawler.bj_ctc (
subNumber VARCHAR(512),
name VARCHAR(512),
department VARCHAR(512),
manager VARCHAR(512),
category VARCHAR(512),
amount VARCHAR(512),
result VARCHAR(512)
);