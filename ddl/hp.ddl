create database if not exists crawler DEFAULT CHARSET utf8;

create table if not exists crawler.hp (
projectName VARCHAR(512),
constLoc VARCHAR(512),
const VARCHAR(512),
ecoEvalDepart VARCHAR(512),
confirmFileName VARCHAR(512),
confirmFileNo VARCHAR(512),
confirmTime VARCHAR(512),
confirmFileLink VARCHAR(512),
postTime VARCHAR(512)
);