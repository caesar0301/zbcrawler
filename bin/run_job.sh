#!/bin/bash
BIN=`dirname $0`
PROHOME=$BIN/..
DEBUG=false
JAR="$PROHOME/target/scala-2.11/zb-crawler-assembly-1.0.jar"

if [ ! -e $JAR ]; then
	echo "run 'sbt assembly' first"
	exit 1
fi

if [ $# -lt 1 ]; then
	echo "Usage: `basename $0` com.cuandas.zbc.[HP|BJ|ZB]Crawler"
	exit 1
fi

java -cp $JAR -Dwebdriver.gecko.driver=$BIN/geckodriver -Dzbcrawler.debug=$DEBUG $@

exit 0;
