create database if not exists crawler DEFAULT CHARSET utf8;

create table if not exists crawler.zb (
searchKey VARCHAR(512),
name VARCHAR(512),
date VARCHAR(512),
category VARCHAR(512),
subNumber VARCHAR(512),
segmentNumber VARCHAR(512),
caller VARCHAR(512),
callerAgency VARCHAR(512),
contact VARCHAR(512),
contactTel VARCHAR(512),
contactAddr VARCHAR(512),
contactPost VARCHAR(512),
subType VARCHAR(512),
segmentName VARCHAR(512),
url VARCHAR(512)
);

create table if not exists crawler.zb_cand (
searchKey VARCHAR(512),
candOrdInt VARCHAR(512),
agency VARCHAR(512),
candOrd VARCHAR(512),
manager VARCHAR(512),
price VARCHAR(512)
);