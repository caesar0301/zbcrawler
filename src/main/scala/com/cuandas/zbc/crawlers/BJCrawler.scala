package com.cuandas.zbc.crawlers

import java.io._
import java.nio.charset.StandardCharsets
import java.sql.DriverManager
import java.util
import java.util.concurrent.TimeUnit

import com.cuandas.zbc.TimeLimitedCodeBlock
import com.cuandas.zbc.Utils._
import org.apache.commons.io.FileUtils
import org.openqa.selenium.{By, NoSuchElementException, WebElement}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.util.matching.Regex

private case class SubmissionInfo
(
  searchKey: String,
  subNumber: String,              // 报建编号
  projectName: String,            // 项目名称
  subTime: String,                // 报建日期
  subCategory: String,            // 项目分类
  permission: String,             // 施工许可

  var const: String = "",         // 建设单位
  var constLocation: String = "", // 建设地点
  var investment: String = "",    // 总投资
  var constScale: String = "",    // 建设规模
  var agency: String = "",        // 报建受理部门

  var contracts: ListBuffer[Contract] = new ListBuffer[Contract]
) {
  def detailAddress: String = {
    s"http://www.ciac.sh.cn/xmbjwsbsweb/xmquery/XmDetail.aspx?bjbh=${subNumber}"
  }

  override def toString = {
    s"${searchKey}|${subNumber}|${projectName}|${subTime}|${subCategory}|${permission}|" +
      s"${const}|${constLocation}|${investment}|${constScale}|${agency}|${detailAddress}"
  }

  def format(): String = {
    "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')".format(
      searchKey,
      subNumber,
      projectName,
      subTime,
      subCategory,
      permission,
      const,
      constLocation,
      investment,
      constScale,
      agency,
      detailAddress
    )
  }
}

private case class Contract
(
  name: String,                   // 合同名称
  department: String,             // 承包单位
  manager: String,                // 项目负责人
  category: String,               // 合同类别
  amount: String,                 // 合同金额
  result: String                  // 报送情况
) {
  def format(subNumber: String): String = {
    "('%s','%s','%s','%s','%s','%s','%s')".format(
      subNumber,
      name,
      department,
      manager,
      category,
      amount,
      result
    )
  }
}

/**
  * 市场项目信息——报建阶段
  */
object BJCrawler extends WebCrawler {
  private val PAGE_SITE = "http://www.shjjw.gov.cn/gb/node2/n9/n719/"
  private val FRAME_SRC = "http://www.ciac.sh.cn/xmbjwsbsweb/xmquery/XmList.aspx"

  private val usage = s"""Usage: ${getClass} jsdw.txt"""
  private val pageIndicatorPattern = new Regex("第(\\d+)页/共(\\d+)页")

  @volatile private var outputFile: String = null
  @volatile private var outputFile2: String = null
  @volatile private var succeededKeysFile: String = null

  if (USE_FILE_BACKEND) {
    outputFile = makeUnifiedOutput("bj.txt")
    outputFile2 = makeUnifiedOutput("bj-cont.txt")
    succeededKeysFile = makeUnifiedOutput("bj.success")
  }

  private val deleteOutputForcedly = false
  @volatile private var succeededKeys = List.empty[String]
  @volatile private var searchKeys = List.empty[String]

  def main(args: Array[String]): Unit = {
    if (USE_FILE_BACKEND) {
      if (args.length == 0) {
        println(usage)
        System.exit(1)
      }
      val jsdw = args.apply(0)
      succeededKeys = Source.fromInputStream(new FileInputStream(new File(succeededKeysFile))).getLines.toList
      searchKeys = Source.fromInputStream(new FileInputStream(new File(jsdw))).getLines.filter { k =>
        !succeededKeys.contains(k)
      }.toList
    } else {
      validateDatabaseContext() // Initialize dbs

      val con = DriverManager.getConnection(JDBC_LINK)
      val stmt = con.createStatement()

      // succeeded keys
      var rs = stmt.executeQuery(s"select * from ${TB_BJ_SUCC}")
      var names = new mutable.ListBuffer[String]
      while(rs.next()) {
        names.append(rs.getString("name"))
      }
      succeededKeys = names.toList
      rs.close()

      // all search keys
      rs = stmt.executeQuery(s"select * from ${TB_JSDW}")
      names = new mutable.ListBuffer[String]
      while(rs.next()) {
        names.append(rs.getString("name"))
      }
      searchKeys = names.toList.filter { k => !succeededKeys.contains(k)}
      rs.close()

      stmt.close()
      con.close()
    }

    if (USE_FILE_BACKEND && deleteOutputForcedly) {
      try {
        FileUtils.forceDelete(new File(outputFile))
        FileUtils.forceDelete(new File(outputFile2))
      } catch {
        case e: FileNotFoundException =>
        case _: Exception =>
      }
    }

    if (isDebugging) {
      searchKeys = searchKeys.slice(0, 3)
    }

    clearRunningContext
    start(searchKeys)
  }

  override def searchInternal(searchKey: String): Unit = {
    logger.info(s"Searching ${searchKey}")
    val subs = new ListBuffer[SubmissionInfo]

    driver.navigate().to(PAGE_SITE)
    val mainWindowId = driver.getWindowHandle
    // Switch to the main frame
    switchToFrameWithTimeout(driver, driver.findElementById("main_content"))

    // Fill in constructor name and search
    val (startDate, endDate) = historyInterval(HISTORY_DAYS_BJ)
    val inputBox = driver.findElementByXPath("//tbody/tr/td/input[@name='txtjsdw']")
    val dateStartBox = driver.findElementByXPath("//tbody/tr/td/input[@name='txtbjqr']")
    val dateEndBox = driver.findElementByXPath("//tbody/tr/td/input[@name='txtbjqrend']")
    val submitButton = driver.findElementByXPath("//tbody/tr/td/input[@name='btnSearch']")
    inputBox.clear()
    inputBox.sendKeys(searchKey)
    dateStartBox.sendKeys(startDate)
    dateEndBox.sendKeys(endDate)

    submitButton.click()
    sleep(CLICK_SNAP)
    // wait.until(ExpectedConditions.visibilityOf(submitButton));

    val total = totalPage()
    var i = 0
    val submissions = new mutable.HashMap[String, SubmissionInfo]()

    while (i < total._2) {
      // Parse entries in main table
      val rows: List[WebElement] = parseRows()
      logger.info(s"Total ${rows.length} submissions found for current page ${i+1}, '${searchKey}'")

      val iter = rows.iterator
      while (iter.hasNext) {
        val row = iter.next
        driver.executeScript("arguments[0].scrollIntoView(true);", row)

        // Parse details of each submission
        var submission = parseSubmissionInfo(searchKey, row)
        if (!submissions.contains(submission.subNumber)) {
          submissions.put(submission.subNumber, submission)
        } else {
          // skip dup items
          submission = null
        }

        if (submission != null) {
          TimeLimitedCodeBlock.runWithTimeout(new Runnable {
            override def run(): Unit = {
              parseSubmissionDetails(row, mainWindowId, submission)
              logger.info(submission.toString)
              subs.append(submission)
            }
          }, HEARTBEAT_TIMEOUT, TimeUnit.MILLISECONDS)
        }
      }

      // Use defaultContent to refer to current active window
      switchToDefaultContentWithTimeout(driver)
      switchToFrameWithTimeout(driver, driver.findElementById("main_content"))

      try {
        val nextPageButton = driver.findElementById("gvXmList_ctl23_lbnNext")
        driver.executeScript("arguments[0].scrollIntoView(true);", nextPageButton)
        if (nextPageButton.getAttribute("disabled") != "disabled") {
          nextPageButton.click()
          sleep(NEXTPAGE_SNAP)
        }
      } catch {
        case e: NoSuchElementException => logger.info("No more pages found")
        case e: Exception => throw e
      }

      i += 1
    }

    succeededKeys = succeededKeys :+ searchKey

    if (USE_FILE_BACKEND) {
      subs.toList.foreach(dumpEntryToFile)
      dumpSucceededKeysToFile(succeededKeys, succeededKeysFile)
    } else {
      dumpEntriesToDatabase(subs.toList)
    }
  }

  private def totalPage(): Tuple2[Int, Int] = {
    try {
      val indicator = driver.findElementById("gvXmList_ctl23_lblPage").getText
      val matched = pageIndicatorPattern.findFirstMatchIn(indicator)
      val current = matched.map { m => m.group(1).toInt }.getOrElse(1)
      val total = matched.map { m => m.group(2).toInt }.getOrElse(1)
      Tuple2(current, total)
    } catch {
      case e: Exception => Tuple2(1, 1)
    }
  }

  private def parseRows(): List[WebElement] = {
    val table = driver.findElementById("gvXmList")
    table.findElements(By.xpath("tbody/tr[@class='gridtd']"))
      .filter { case row: WebElement =>
        val entries = row.findElements(By.xpath("td"))
        entries.nonEmpty
      }.toList
  }

  private def swithToPopupWindow(mainWindowId: String) = {
    val popupWindowIds = driver.getWindowHandles.filter(_ != mainWindowId)
    if (popupWindowIds.toArray.length == 0)
      throw new RuntimeException("No popup window found")
    val popupWindow = popupWindowIds.head
    switchToWindowWithTimeout(driver, popupWindow)
  }

  private def parseSubmissionInfo(searchKey: String, row: WebElement): SubmissionInfo = {
    val tds = try {
      row.findElements(By.xpath("td")).toList
    } catch {
      case e: Exception =>
        logger.error(e.toString :: e.getStackTraceString :: Nil mkString("\n"))
        List.empty[WebElement]
    }
    if (tds.nonEmpty) {
      SubmissionInfo(
        searchKey = searchKey,
        subNumber = tds.apply(0).getText,
        projectName = tds.apply(1).getText,
        subTime = tds.apply(2).getText,
        subCategory = tds.apply(3).getText,
        permission = tds.apply(4).getText
      )
    } else {
      null
    }
  }

  private def parseSubmissionDetails(row: WebElement, mainWindowId: String, submission: SubmissionInfo) = {
    // Click row and switch to pop-upped window
    row.findElement(By.tagName("a")).click()
    withSnapRetries(1000) { () => swithToPopupWindow(mainWindowId) }

    // Locate tabs
    var tabs = util.Arrays.asList[WebElement]()
    var table: WebElement = null
    withSnapRetries(500) { () => {
      tabs = driver.findElementsByXPath("//div[@class='tabs-wrap']/ul[@class='tabs']/li")
      if (tabs.size != 4) {
        throw new RuntimeException(s"Incorrect number of tabs: ${tabs.size}")
      }
    }}

    //项目报建
    logger.debug("Parsing project detail")
    // tabs.get(0).click()
    withSnapRetries(500) { () => {
      try {
        switchToFrameWithTimeout(driver,
          driver.findElement(By.xpath(s"//iframe[@src='Xmxx.aspx?bjbh=${submission.subNumber}']"))
        )
        table = driver.findElementByXPath("//form[@name='form1']//table")
        if (table == null)
          throw new RuntimeException("Could not find table")
        parseSubmissionSummary(table, submission)
      } catch {
        case e: Exception => switchToDefaultContentWithTimeout(driver)
          throw e
      }
    }}
    switchToDefaultContentWithTimeout(driver)

    //合同信息报送
    logger.debug("Parsing contract detail")
    tabs.get(1).click()
    withSnapRetries(2000) { () => {
      try {
        switchToFrameWithTimeout(driver,
          driver.findElement(By.xpath(s"//iframe[@src='HtList.aspx?bjbh=${submission.subNumber}']"))
        )
      } catch {
        case e: Exception => switchToDefaultContentWithTimeout(driver)
          throw e
      }
    }}
    withSnapRetries(500) { () => {
      table = driver.findElementByXPath("//div[@class='datagrid-view2']/div[@class='datagrid-body']" +
        "/table[@class='datagrid-btable']"
      )
      if (table == null)
        throw new RuntimeException("Could not find table")
    }}
    val contracts = parseContracts(table)
    contracts.foreach { c => submission.contracts.append(c) }
    switchToDefaultContentWithTimeout(driver)

    // (Optional) close pop-upped window
    driver.close()

    // Switch back to original window
    switchToWindowWithTimeout(driver, mainWindowId)
  }

  private def parseSubmissionSummary(table: WebElement, submission: SubmissionInfo): Unit = {
    val entries = table.findElements(By.xpath("tbody/tr"))
    val details = entries.toArray.map { case entry: WebElement =>
      val key = entry.findElement(By.className("td1_title")).getText
      val value = entry.findElement(By.className("td1")).getText
      logger.debug(s"${key}: ${value}")
      value
    }
    submission.const = details(2)
    submission.constLocation = details(3)
    submission.investment = details(4)
    submission.constScale = details(5)
    submission.agency = details(6)
  }

  private def parseContracts(table: WebElement): List[Contract] = {
    val entries = table.findElements(By.xpath("tbody/tr"))
    entries.toArray.map { case entry: WebElement =>
      val xmmc = entry.findElement(By.xpath("td[@field='xmmc']")).getText
      val cbdws = entry.findElement(By.xpath("td[@field='cbdws']")).getText
      val xmfzr = entry.findElement(By.xpath("td[@field='xmfzr']")).getText
      val htlb = entry.findElement(By.xpath("td[@field='htlb']")).getText
      val htje = entry.findElement(By.xpath("td[@field='htje']")).getText
      val fb = entry.findElement(By.xpath("td[@field='fb']")).getText
      logger.debug(s"${xmmc},${cbdws},${xmfzr},${htlb},${htje},${fb}")
      Contract(
        name = xmmc,
        department = cbdws,
        manager = xmfzr,
        category = htlb,
        amount = htje,
        result = fb
      )
    }.toList
  }

  private def dumpEntryToFile(submission: SubmissionInfo): Unit = {
    val of = new OutputStreamWriter(new FileOutputStream(outputFile, true), StandardCharsets.UTF_8)
    of.write(submission.toString.replaceAll("[\\r\\n]+", "&*&") + "\n")
    of.close()

    val of2 = new OutputStreamWriter(new FileOutputStream(outputFile2, true), StandardCharsets.UTF_8)
    submission.contracts.foreach { c =>
      of2.write(s"%s|%s|%s|%s|%s|%s|%s"
        .format(submission.subNumber, c.name,
          c.department, c.manager, c.category, c.amount, c.result)
        .replaceAll("[\\r\\n]+", "&*&")
        + "\n"
      )
    }
    of2.close()
  }

  private def dumpEntriesToDatabase(submission: List[SubmissionInfo]): Unit = {
    val con = DriverManager.getConnection(JDBC_LINK)
    val stmt = con.createStatement()

    try {
      // dump results
      submission.foreach { s =>
        val cmd = if (isMysqlBackend) {
          s"INSERT INTO ${TB_BJ} VALUES ${s.format()} ON DUPLICATE KEY UPDATE investment='${s.investment}'"
        } else {
          s"INSERT INTO ${TB_BJ} VALUES ${s.format()}"
        }
        executeUpsertSilently(stmt, cmd)

        s.contracts.foreach { c =>
          val subNumber = s.subNumber
          val cmd = if (isMysqlBackend) {
            s"INSERT INTO ${TB_BJ_CTC} VALUES ${c.format(subNumber)} ON DUPLICATE KEY UPDATE result='${c.result}'"
          } else {
            s"INSERT INTO ${TB_BJ_CTC} VALUES ${c.format(subNumber)}"
          }
          executeUpsertSilently(stmt, cmd)
        }
      }

      // update running status
      stmt.executeUpdate(s"TRUNCATE TABLE ${TB_BJ_SUCC}")
      val values = succeededKeys.map("('%s')".format(_)).mkString(",")
      stmt.executeUpdate(s"INSERT INTO ${TB_BJ_SUCC} VALUES ${values}")
    } finally {
      stmt.close()
      con.close()
    }
  }
}
