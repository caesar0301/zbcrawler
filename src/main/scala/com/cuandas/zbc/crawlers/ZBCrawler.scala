package com.cuandas.zbc.crawlers

import java.io.{File, FileInputStream, FileOutputStream, OutputStreamWriter}
import java.nio.charset.StandardCharsets
import java.sql.DriverManager
import java.util.concurrent.TimeUnit

import com.cuandas.zbc.TimeLimitedCodeBlock
import com.cuandas.zbc.Utils._
import org.apache.commons.io.FileUtils
import org.openqa.selenium.{By, WebElement}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

case class ZBInfo
(
  searchKey: String,
  name: String,
  date: String,
  category: String,
  var subNumber: String = "",       // 报建编号
  var segmentNumber: String = "",   // 标段号
  var caller: String = "",          // 招标人
  var callerAgency: String = "",    // 招标代理机构
  var contact: String = "",         // 联系人
  var contactTel: String = "",      // 联系人电话
  var contactAddr: String = "",     // 联系人地址
  var contactPost: String = "",     // 联系人邮编
  var subType: String ="",          // 招标方式
  var segmentName: String = "",     // 招标标段名称
  var tbinfo: List[TBInfo] = List.empty[TBInfo],
  var pageAddr: String = ""
) {
  def format(): String = {
    "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')".format(
      searchKey,
      name,
      date,
      category,
      subNumber,
      segmentNumber,
      caller,
      callerAgency,
      contact,
      contactTel,
      contactAddr,
      contactPost,
      subType,
      segmentName,
      pageAddr
    )
  }
}

case class TBInfo
(
  agency: String,   // 投标人
  order: String,    // 中标候选人排序
  manager: String,  // 项目负责人姓名
  price: String     // 投标报价(万元)
) {
  def format(subNumber: String, orderInt: Int): String = {
    "('%s','%s','%s','%s','%s','%s')".format(
      subNumber,
      orderInt.toString,
      agency,
      order,
      manager,
      price
    )
  }
}

/**
  * 招标信息
  */
object ZBCrawler extends WebCrawler {

  private val PAGE_SITE = "http://www.shjjw.gov.cn/gb/node2/n4/n14/n840/n845/u1ai173078.html"
  private val usage = s"""Usage: ${getClass} jldw.txt""".stripMargin

  @volatile private var outputFile: String = null
  @volatile private var outputFile2: String = null
  @volatile private var succeededKeysFile: String = null

  if (USE_FILE_BACKEND) {
    outputFile = makeUnifiedOutput("zb.txt")
    outputFile2 = makeUnifiedOutput("zb-houxuan.txt")
    succeededKeysFile = makeUnifiedOutput("zb.success")
  }

  private val deleteOutputForcedly = false
  @volatile private var mainWindowId: String = _
  @volatile private var succeededKeys = List.empty[String]
  @volatile private var searchKeys = List.empty[String]

  def main(args: Array[String]): Unit = {

    if (USE_FILE_BACKEND) {
      if (args.length > 0) {
        val jldw = args.apply(0)
        succeededKeys = Source.fromInputStream(new FileInputStream(new File(succeededKeysFile))).getLines.toList
        searchKeys = Source.fromInputStream(new FileInputStream(new File(jldw))).getLines.filter { k =>
          !succeededKeys.contains(k)
        }.toList
      }
    } else {
      validateDatabaseContext()

      val con = DriverManager.getConnection(JDBC_LINK)
      val stmt = con.createStatement()
      // succeeded keys
      var rs = stmt.executeQuery(s"select * from ${TB_ZB_SUCC}")
      var names = new mutable.ListBuffer[String]
      while(rs.next()) {
        names.append(rs.getString("name"))
      }
      succeededKeys = names.toList
      rs.close()
      // all search keys
      rs = stmt.executeQuery(s"select * from ${TB_JLDW}")
      names = new mutable.ListBuffer[String]
      while(rs.next()) {
        names.append(rs.getString("name"))
      }
      searchKeys = names.toList.filter { k => !succeededKeys.contains(k)}
      rs.close()
      stmt.close()
      con.close()
    }

    if (isDebugging) {
      logger.warn("Empty search keys given")
      searchKeys = searchKeys.slice(0, 3)
    }

    if (USE_FILE_BACKEND && deleteOutputForcedly) {
      try {
        FileUtils.forceDelete(new File(outputFile))
        FileUtils.forceDelete(new File(outputFile2))
      } catch {
        case e: Exception =>
      }
    }

    clearRunningContext
    start(searchKeys)
  }

  override def searchInternal(searchKey: String): Unit = {
    logger.info(s"Searching '${searchKey}'")
    val result = new ListBuffer[ZBInfo]

    driver.navigate().to(PAGE_SITE)
    mainWindowId = driver.getWindowHandle
    switchToFrameWithTimeout(driver, driver.findElementById("mainframe"))

    // Fill in constructor name and search
    val (startDate, endDate) = historyInterval(HISTORY_DAYS_ZB)
    val inputBox = driver.findElementByXPath("//form[@id='form1']//input[@id='txtzbhxr']")
    val dateStartBox = driver.findElementByXPath("//tbody/tr/td/input[@name='txtgsrq']")
    val dateEndBox = driver.findElementByXPath("//tbody/tr/td/input[@name='txtTogsrq']")
    inputBox.clear()
    inputBox.sendKeys(searchKey)
    dateStartBox.sendKeys(startDate)
    dateEndBox.sendKeys(endDate)
    val submitButton = driver.findElementById("Button1")
    submitButton.click()

    sleep(CLICK_SNAP)
    heartbeat()

    var hasNextPage = true
    var currentPage = 0
    while(hasNextPage) {
      currentPage += 1
      var rows: List[WebElement] = parseRows()
      logger.info(s"Total ${rows.length} entries found for current page of ${currentPage}")

      // TODO: process row entry
      var i = 0
      val total = rows.length
      while (i < total) {
        val row = rows.apply(i)
        driver.executeScript("arguments[0].scrollIntoView(true);", row)

        val zb = parseZBRow(searchKey, row)
        if (zb != null) {

          TimeLimitedCodeBlock.runWithTimeout(new Runnable {
            override def run(): Unit = {
              // Click row and switch to pop-upped window
              row.findElement(By.tagName("a")).click()
              withSnapRetries(6000) { () => swithToLatestPopupWindow() }

              // Do stuff
              parseEntryDetailPage(zb)
              driver.close()

              // Switch back to original window
              switchToWindowWithTimeout(driver, mainWindowId)
              rows = parseRows() // Re-parse due to the update of original page
              logger.info(s"Switched to main window ${mainWindowId} title ${driver.getTitle}")

              logger.info(zb.toString)
              result.append(zb)
            }
          }, HEARTBEAT_TIMEOUT, TimeUnit.MILLISECONDS)

        }
        i += 1
      }

      // Use defaultContent to refer to current active window
      switchToDefaultContentWithTimeout(driver)
      switchToFrameWithTimeout(driver, driver.findElementById("mainframe"))

      // Nevigate to next page
      val pagination = driver.findElementById("gvList").findElements(By.xpath("tbody/tr"))
        .filter(_.getAttribute("class") == "pagestyle")
      if (pagination.nonEmpty) {
        // Scroll down to activate pagination (this really a hack!!!!)
        driver.executeScript("arguments[0].scrollIntoView(true);", pagination.head)
        val pages = pagination.head.findElements(By.xpath("td/table/tbody/tr/td"))
        if (pages.nonEmpty) {
          val maxPage = pages.map(_.getText.toInt).max
          currentPage = pages.filter(_.findElements(By.xpath("a")).isEmpty).head.getText.toInt
          if (currentPage == maxPage) {
            hasNextPage = false
          } else {
            pages(currentPage).findElement(By.xpath("a")).click()
            sleep(NEXTPAGE_SNAP)
          }
        }
      } else {
        hasNextPage = false
      }
    }

    succeededKeys = succeededKeys :+ searchKey

    if (USE_FILE_BACKEND) {
      result.toList.foreach(dumpEntryToFile)
      dumpSucceededKeysToFile(succeededKeys, succeededKeysFile)
    } else {
      dumpEntriesToDatabase(result.toList)
    }
  }

  private def parseRows(): List[WebElement] = {
    val table = driver.findElementById("gvList")
    table.findElements(By.xpath("tbody/tr"))
      .filter { case row: WebElement =>
        val tds = row.findElements(By.xpath("td"))
        val cls = row.getAttribute("class")
        cls != "gridHead" && cls != "pagestyle" && tds.length >= 3
      }.toList
  }

  private def parseZBRow(searchKey: String, row: WebElement): ZBInfo = {
    val tds = try {
      row.findElements(By.xpath("td")).toList
    } catch {
      case e: Exception =>
        logger.error(e.toString :: e.getStackTraceString :: Nil mkString("\n"))
        List.empty[WebElement]
    }
    if (tds.nonEmpty) {
      ZBInfo(
        searchKey = searchKey,
        name = tds.apply(0).getText,
        date = tds.apply(1).getText,
        category = tds.apply(2).getText
      )
    } else {
      null
    }
  }

  private def swithToLatestPopupWindow() = {
    val popupWindowIds = driver.getWindowHandles.filter(_ != mainWindowId)
    if (popupWindowIds.toArray.length == 0)
      throw new RuntimeException("No popup window found")
    val popupWindow = popupWindowIds.toList(popupWindowIds.size - 1)
    switchToWindowWithTimeout(driver, popupWindow)
    logger.info(s"Switched to popup window ${popupWindow}, title ${driver.getTitle}")
  }

  private def parseEntryDetailPage(zb: ZBInfo): Unit = {
    val mainForm = driver.findElementById("form1")
    val mainTable = mainForm.findElement(By.xpath("div[@class='contents']/div[@class='main']/table[@class='table01']"))

    zb.subNumber = mainTable.findElement(By.id("lblbjbh")).getText
    zb.segmentNumber = mainTable.findElement(By.id("Label1")).getText
    zb.caller = mainTable.findElement(By.id("Label2")).getText
    zb.callerAgency = mainTable.findElement(By.id("Label4")).getText
    zb.contact = mainTable.findElement(By.id("Label5")).getText
    zb.contactTel = mainTable.findElement(By.id("Label7")).getText
    zb.contactAddr = mainTable.findElement(By.id("Label8")).getText
    zb.contactPost = mainTable.findElement(By.id("Label9")).getText
    zb.subType = mainTable.findElement(By.id("lblzbfs")).getText
    zb.segmentName = mainTable.findElement(By.id("Label6")).getText
    zb.pageAddr = driver.getCurrentUrl
    val tbListTable = { // 招投标情况
      val smallTable = mainTable.findElements(By.xpath("tbody/tr")).map { r =>
        val tds = r.findElements(By.tagName("td"))
        if (tds.length >= 2) {
          if (tds.apply(0).getText.replaceAll("[\\s：]+", "") == "招投标情况") {
            tds.apply(1).findElement(By.xpath("table"))
          } else {
            null
          }
        } else {
          null
        }
      }.filter(_ != null)
      if (smallTable.nonEmpty) {
        smallTable.head
      } else {
        throw new RuntimeException("TB list not found")
      }
    }
    parseTBList(tbListTable, zb)

    val toPublicLink = {    // 中标候选人公示详细信息
      val anchor = mainTable.findElements(By.xpath("tbody/tr")).map { r =>
        val tds = r.findElements(By.tagName("td"))
        if (tds.length >= 2) {
          if (tds.apply(0).getText.replaceAll("[\\s：]+", "") == "详细") {
            tds.apply(1).findElement(By.tagName("a"))
          } else {
            null
          }
        } else {
          null
        }
      }.filter(_ != null)
      if (anchor.nonEmpty) {
        anchor.head
      } else {
        throw new RuntimeException("Anchor not found")
      }
    }
    // toPublicLink.click()
  }

  private def parseTBList(smallTable: WebElement, zb: ZBInfo): Unit = {
    val rows = smallTable.findElements(By.xpath("tbody/tr"))
    val upper = rows.length -1
    val tblist =
      try {
        zb.segmentNumber.charAt(0) match {
          case 'A' | 'B' => {
            rows.slice(1, upper).map { row =>
              val tds = row.findElements(By.tagName("td"))
              if (isDebugging) {
                assert(tds.length == 4)
              }
              TBInfo(
                agency = tds(0).getText,
                order = tds(1).getText,
                manager = "",
                price = tds(3).getText
              )
            }.toList
          }
          case 'C' | 'Y' => {
            if (zb.segmentNumber.startsWith("YY")) {
              rows.slice(2, upper).map { row =>
                val tds = row.findElements(By.tagName("td"))
                TBInfo(
                  agency = tds(0).getText,
                  order = tds(1).getText,
                  manager = tds(2).getText,
                  price = tds(4).getText
                )
              }.toList
            } else {
              rows.slice(1, upper).map { row =>
                val tds = row.findElements(By.tagName("td"))
                if (isDebugging) {
                  assert(tds.length == 6)
                }
                TBInfo(
                  agency = tds(0).getText,
                  order = tds(1).getText,
                  manager = tds(2).getText,
                  price = tds(5).getText
                )
              }.toList
            }
          }
          case 'F' | 'J' => {
            rows.slice(1, upper).map { row =>
              val tds = row.findElements(By.tagName("td"))
              if (isDebugging) {
                assert(tds.length == 5)
              }
              TBInfo(
                agency = tds(0).getText,
                order = tds(1).getText,
                manager = tds(2).getText,
                price = tds(4).getText
              )
            }.toList
          }
          case 'U' => {
            rows.slice(2, upper).map { row =>
              val tds = row.findElements(By.tagName("td"))
              TBInfo(
                agency = tds(0).getText,
                order = tds(1).getText,
                manager = "",
                price = tds(3).getText
              )
            }.toList
          }
          case 'W' | 'V' => {
            rows.slice(2, upper).map { row =>
              val tds = row.findElements(By.tagName("td"))
              TBInfo(
                agency = tds(0).getText,
                order = tds(1).getText,
                manager = tds(2).getText,
                price = tds(4).getText
              )
            }.toList
          }
          case _ =>
            throw new RuntimeException(s"Unknown type ${zb.segmentNumber}")
        }
      } catch {
        case e: Exception =>
          // TODO: fix pattern mismatching with flexible table extraction
          logger.error(e.toString)
          List.empty[TBInfo]
      }
    zb.tbinfo = tblist
  }

  private def dumpEntryToFile(zb: ZBInfo): Unit = {
    val of = new OutputStreamWriter(new FileOutputStream(outputFile, true), StandardCharsets.UTF_8)
    of.write(s"${zb.searchKey}|${zb.name}|${zb.date}|${zb.category}|" +
      s"${zb.subNumber}|${zb.segmentNumber}|${zb.caller}|${zb.callerAgency}|" +
      s"${zb.contact}|${zb.contactTel}|${zb.contactAddr}|${zb.contactPost}|" +
      s"${zb.subType}|${zb.segmentName}|${zb.pageAddr}" + "\n")
    of.close()

    val of2 = new OutputStreamWriter(new FileOutputStream(outputFile2, true), StandardCharsets.UTF_8)
    val subNumber = zb.subNumber
    var i = 0
    while (i < zb.tbinfo.length) {
      val c = zb.tbinfo(i)
      of2.write(s"${subNumber}|${i}|${c.agency}|${c.order}|${c.manager}|${c.price}" + "\n")
      i += 1
    }
    of2.close()
  }

  private def dumpEntriesToDatabase(zbs: List[ZBInfo]): Unit = {
    val con = DriverManager.getConnection(JDBC_LINK)
    val stmt = con.createStatement()

    try {
      // dump results
      zbs.foreach { zb =>
        val cmd = if (isMysqlBackend) {
          s"INSERT INTO ${TB_ZB} VALUES ${zb.format()} ON DUPLICATE KEY UPDATE name='${zb.name}'"
        } else {
          s"INSERT INTO ${TB_ZB} VALUES ${zb.format()}"
        }
        executeUpsertSilently(stmt, cmd)

        val subNumber = zb.subNumber
        var i = 0
        while (i < zb.tbinfo.length) {
          val c = zb.tbinfo(i)
          val cmd = if (isMysqlBackend) {
            s"INSERT INTO ${TB_ZB_CAND} VALUES ${c.format(subNumber, i)} ON DUPLICATE KEY UPDATE agency='${c.agency}'"
          } else {
            s"INSERT INTO ${TB_ZB_CAND} VALUES ${c.format(subNumber, i)}"
          }
          executeUpsertSilently(stmt, cmd)
          i += 1
        }
      }

      // update running status
      stmt.executeUpdate(s"TRUNCATE TABLE ${TB_ZB_SUCC}")
      val values = succeededKeys.filter { k => k != null && k.nonEmpty }
        .map("('%s')".format(_)).mkString(",")
      stmt.executeUpdate(s"INSERT INTO ${TB_ZB_SUCC} VALUES ${values}")
    } finally {
      stmt.close()
      con.close()
    }
  }
}
