package com.cuandas.zbc

import java.io.{File, PrintWriter}
import java.net.URL
import java.sql.{Connection, DriverManager, Statement}
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.microsoft.sqlserver.jdbc.SQLServerException
import com.typesafe.scalalogging.LazyLogging
import org.jutils.jprocesses.JProcesses
import org.jutils.jprocesses.model.ProcessInfo
import org.openqa.selenium.WebElement
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.phantomjs.{PhantomJSDriver, PhantomJSDriverService}
import org.openqa.selenium.remote.{DesiredCapabilities, RemoteWebDriver}

object BrowserType extends Enumeration {
  type BrowserType = Value
  val FIREFOX, IE, EDGE, HTMLUNIT, PHANTOMJS, UNDEFINED = Value

  def withName(s: String, default: BrowserType): BrowserType = {
    try {
      BrowserType.withName(s.toUpperCase)
    } catch {
      case e: Exception => default
    }
  }
}

object Utils extends LazyLogging {

  type OptionMap = Map[Symbol, Any]

  val isDebugging = System.getProperty("zbcrawler.debug", "false").toBoolean
  val ALWAYS_REFRESH_START = System.getProperty("zbcrawler.refresh.start", "false").toBoolean
  val DRIVER_TIMEOUT = System.getProperty("zbcrawler.driver.timeout", "180000").toInt

  val BROWSER_DRIVER = BrowserType.withName(System.getProperty("zbcrawler.browser", "firefox"), BrowserType.UNDEFINED)
  logger.info(s"Detecting browser type: ${BROWSER_DRIVER}")
  val SELENIUM_SERVER = System.getProperty("zbcrawler.selenium.server", "localhost")
  val PHANTOMJS_EXE = System.getProperty("zbcrawler.phantomjs.executable", "driver/phantomjs")

  // Retry snap parameters
  val RETRIES_LIMIT = System.getProperty("zbcrawler.retries", "5").toInt
  val RETRY_SNAP = System.getProperty("zbcrawler.retry.snap", "3000").toInt
  val RETRY_SNAP_STEP = System.getProperty("zbcrawler.retry.snap.step", "1000").toInt
  val RETRY_SNAP_MAX = System.getProperty("zbcrawler.retry.snap.max", "10000").toInt
  val CLICK_SNAP = System.getProperty("zbcrawler.click.snap", "5000").toInt
  val NEXTPAGE_SNAP = System.getProperty("zbcrawler.nextpage.snap", "2000").toInt

  // Smart switches
  val SWITCH_TIMEOUT = System.getProperty("zbcrawler.switch.timeout", "60000").toInt
  val REPEAT_PERIOD = System.getProperty("zbcrawler.repeat.period", "86400").toInt
  val HEARTBEAT_TIMEOUT = System.getProperty("zbcrawler.heartbeat.timeout", "30000").toInt

  val USE_FILE_BACKEND = false
  val FILE_BACKEND_OUTDIR = System.getProperty("zbcrawler.output.dir", "crawler.out")
  val FILTER_HP_CONSTRUCTORS = System.getProperty("zbcrawler.filter.hp", "true").toBoolean

  val HISTORY_DAYS = System.getProperty("zbcrawler.history.days", "90").toInt
  val HISTORY_DAYS_HP = System.getProperty("zbcrawler.history.days.hp", HISTORY_DAYS.toString).toInt
  val HISTORY_DAYS_ZB = System.getProperty("zbcrawler.history.days.zb", HISTORY_DAYS.toString).toInt
  val HISTORY_DAYS_BJ = System.getProperty("zbcrawler.history.days.bj", HISTORY_DAYS.toString).toInt

//  private val JDBC_DEFAULT = "jdbc:mysql://localhost:3306/crawler?user=root&password=password"
  private val JDBC_DEFAULT = "jdbc:sqlserver://localhost:1433;user=admin;password=password"
  val JDBC_LINK = System.getProperty("zbcrawler.jdbc.link", JDBC_DEFAULT)

  val DB_NAME = "crawler"
  val TB_HP = "crawler.hp"
  val TB_BJ = "crawler.bj"
  val TB_BJ_CTC = "crawler.bj_ctc"
  val TB_BJ_SUCC = "crawler.bj_success"
  val TB_ZB = "crawler.zb"
  val TB_ZB_CAND = "crawler.zb_cand"
  val TB_ZB_SUCC = "crawler.zb_success"
  val TB_JSDW = "crawler.jsdw"
  val TB_JLDW = "crawler.jldw"

  /**
    * An automatic logic to retry parsing utility
    */
  def withSnapRetries(msec: Long)(func: () => Unit): Unit = {
    // The max snap before a utility encounters a failure
    var succeeded = false
    var snap = msec
    while (!succeeded) {
      try {
        sleep(snap)
        func()
        succeeded = true
      } catch {
        case e: Exception => {
          if (snap > RETRY_SNAP_MAX) {
            throw e
          } else {
            logger.warn((e.getMessage :: e.getStackTrace.map(_.toString) :: Nil).mkString("\n"))
            snap += RETRY_SNAP_STEP
          }
        }
      }
    }
  }

  def sleep(msecs: Long): Unit = {
    Thread.sleep(msecs)
    logger.debug(s"Sleeping ${msecs} msec ...")
  }

  def makeUnifiedOutput(file: String): String = {
    val outdir = new File(FILE_BACKEND_OUTDIR)
    if (!outdir.exists()) {
      logger.info(s"Output directory '${FILE_BACKEND_OUTDIR}' not found")
      try {
        outdir.mkdir()
        logger.info(s"'${FILE_BACKEND_OUTDIR}' created!")
      } catch {
        case e: Exception =>
          logger.error(s"Failed to create directory '${FILE_BACKEND_OUTDIR}'")
          throw e
      }
    }
    val outFileName = s"${FILE_BACKEND_OUTDIR}/${file}"
    val outfile = new File(outFileName)
    if (!outfile.exists()) {
      try {
        outfile.createNewFile()
      } catch {
        case e: Exception =>
          logger.error(s"Failed to create file '${outFileName}'")
          throw e
      }
    }
    outFileName
  }

  def clearRunningContext(): Unit = {
    if (BROWSER_DRIVER == BrowserType.FIREFOX) {
      val processesList = JProcesses.getProcessList()
      val pids = processesList.toArray.filter { case pinfo: ProcessInfo =>
        val pname = pinfo.getName.toLowerCase
        pname.contains("firefox") || pname.contains("geckodriver") || pname.contains("werfault")
      }.map { case pinfo: ProcessInfo => pinfo.getPid }

      if (pids.nonEmpty) {
        logger.info(s"Clear running context")
        pids.foreach { pid =>
          try {
            JProcesses.killProcess(Integer.valueOf(pid)).isSuccess
          } catch {
            case e: Throwable =>
          }
        }
      }
    }
  }

  def createWebDriver(): RemoteWebDriver = {
    val driver = BROWSER_DRIVER match {
      case BrowserType.FIREFOX => new FirefoxDriver()
      case BrowserType.IE => {
        val capabilities = DesiredCapabilities.internetExplorer()
        capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true)
        capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true)
        capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, false)
        new InternetExplorerDriver(capabilities)
      }
      case BrowserType.EDGE => new EdgeDriver()
      case BrowserType.HTMLUNIT => {
        // FIXME: illegal character
        val capabilities = DesiredCapabilities.htmlUnitWithJs()
        new RemoteWebDriver(new URL(s"http://${SELENIUM_SERVER}:4444/wd/hub"), capabilities)
      }
      case BrowserType.PHANTOMJS => {
        // FIXME: Element does not exist in cache
        val capabilities = new DesiredCapabilities()
        capabilities.setJavascriptEnabled(true)
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, PHANTOMJS_EXE)
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX,"Y")
        // capabilities.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0")
        new PhantomJSDriver(capabilities)
      }
      case BrowserType.UNDEFINED =>
        throw new RuntimeException("Unsupported browser type")
    }
    // driver.manage().timeouts().pageLoadTimeout(DRIVER_TIMEOUT, TimeUnit.MILLISECONDS)
    driver
  }

  def stopDriverQuietly(driver: RemoteWebDriver, t: Throwable = null): Unit = {
    if (t != null) {
      logger.error((t.getMessage :: t.getStackTrace.map(_.toString) :: Nil).mkString("\n"))
    }
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          logger.info("Stopping driver quietly, maybe!")
          driver.close()
          sleep(1000)

          // Check browser processes manually
          if (BROWSER_DRIVER == BrowserType.FIREFOX) {
            val processesList = JProcesses.getProcessList()
            val pids = processesList.toArray.filter { case pinfo: ProcessInfo =>
              pinfo.getName.toLowerCase.contains("firefox")
            }.map { case pinfo: ProcessInfo => pinfo.getPid }

            if (pids.nonEmpty) {
              logger.info(s"Firefox pids: ${pids.mkString(", ")}. Kill forcedly")
              pids.foreach { pid =>
                val success = try {
                  JProcesses.killProcess(Integer.valueOf(pid)).isSuccess
                } catch {
                  case e: Throwable => false
                }
                logger.info(s"Killing pid ${pid}: ${if (success) "succeeded" else "failed"}")
              }
              // driver.quit()
            }
          } else {
            driver.quit()
          }
        }
      }, 30, TimeUnit.SECONDS)
    } catch {
      case e: Exception => logger.error(e.toString)
    } finally {
      Thread.sleep(2000)
      clearRunningContext()
    }
  }

  def switchToWindowWithTimeout(driver: RemoteWebDriver, windowId: String): Unit = {
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          driver.switchTo().window(windowId)
          logger.debug(s"Switched to window ${windowId}, title ${driver.getTitle}")
        }
      }, SWITCH_TIMEOUT, TimeUnit.MILLISECONDS)
    } catch {
      case e: Exception => throw e
    }
  }

  def switchToFrameWithTimeout(driver: RemoteWebDriver, frame: WebElement): Unit = {
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          driver.switchTo().frame(frame)
          logger.debug(s"Switched to frame, title ${driver.getTitle}")
        }
      }, SWITCH_TIMEOUT, TimeUnit.MILLISECONDS)
    } catch {
      case e: Exception => throw e
    }
  }

  def switchToDefaultContentWithTimeout(driver: RemoteWebDriver): Unit = {
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          driver.switchTo().defaultContent()
          logger.debug("Switched to default content")
        }
      }, SWITCH_TIMEOUT, TimeUnit.MILLISECONDS)
    } catch {
      case e: Exception => throw e
    }
  }

  def dumpSucceededKeysToFile(succ: List[String], outfile: String): Unit = {
    // Record succeeded keys
    val writer = new PrintWriter(outfile, "utf-8")
    succ.foreach { p =>
      writer.println("%s".format(p.toString))
    }
    writer.close()
  }

  def isMysqlBackend: Boolean = {
    JDBC_LINK.split(":").apply(1).toLowerCase == "mysql"
  }

  def isSqlServerBackend: Boolean = {
    JDBC_LINK.split(":").apply(1).toLowerCase == "sqlserver"
  }

  def clearSuccessStatusTables(tables: List[String]): Unit = {
    var con: Connection = null
    var stmt: Statement = null
    try {
      con = DriverManager.getConnection(JDBC_LINK)
      stmt = con.createStatement()

      if (isMysqlBackend) {
        Class.forName("com.mysql.jdbc.Driver").newInstance()
      } else if (isSqlServerBackend) {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance()
      } else {
        throw new RuntimeException(s"Unsupported jdbc protocol: ${JDBC_LINK}")
      }

      tables.foreach { t =>
        stmt.executeUpdate(s"TRUNCATE TABLE ${t}")
      }
    } finally {
      if (con != null)
        con.close()
      if (stmt != null)
        stmt.close()
    }
  }

  def validateDatabaseContext(): Unit = {
    var con: Connection = null
    var stmt: Statement = null
    try {
      con = DriverManager.getConnection(JDBC_LINK)
      stmt = con.createStatement()

      if (isMysqlBackend) {
        logger.info("Validating database as of MySQL ...")
        Class.forName("com.mysql.jdbc.Driver").newInstance()
        createTablesMysql(stmt)
      } else if (isSqlServerBackend) {
        logger.info("Validating database as of MS SQLServer ...")
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance()
        createTablesSqlServer(stmt)
      } else {
        throw new RuntimeException(s"Unsupported jdbc protocol: ${JDBC_LINK}")
      }
    } finally {
      if (con != null)
        con.close()
      if (stmt != null)
        stmt.close()
    }
  }

  private def createTablesMysql(stmt: Statement): Unit = {
    // Create backend database
    stmt.executeUpdate(s"CREATE DATABASE IF NOT EXISTS ${DB_NAME} " +
      "DEFAULT CHARSET 'utf8' COLLATE 'utf8_general_ci'")

    // HP tables
    val qTableHp =
      s"""CREATE TABLE IF NOT EXISTS ${TB_HP} (
        projectName VARCHAR(255) PRIMARY KEY,
        constLoc VARCHAR(512),
        const VARCHAR(512),
        ecoEvalDepart VARCHAR(512),
        confirmFileName VARCHAR(512),
        confirmFileNo VARCHAR(512),
        confirmTime VARCHAR(512),
        confirmFileLink VARCHAR(512),
        postTime VARCHAR(512)
        )
        DEFAULT CHARSET utf8
        """.stripMargin
    stmt.executeUpdate(qTableHp)

    // BJ tables
    val qTableBj =
      s"""CREATE TABLE IF NOT EXISTS ${TB_BJ} (
        searchKey VARCHAR(512),
        subNumber VARCHAR(255) PRIMARY KEY,
        projectName VARCHAR(512),
        subTime VARCHAR(512),
        subCategory VARCHAR(512),
        permission VARCHAR(512),
        const VARCHAR(512),
        constLoc VARCHAR(512),
        investment VARCHAR(512),
        constScale VARCHAR(512),
        agency VARCHAR(512),
        url VARCHAR(512)
        )
        DEFAULT CHARSET utf8
      """.stripMargin
    stmt.executeUpdate(qTableBj)

    val qTableBjCont =
      s"""CREATE TABLE IF NOT EXISTS ${TB_BJ_CTC} (
        subNumber VARCHAR(255),
        name VARCHAR(512),
        department VARCHAR(255),
        manager VARCHAR(512),
        category VARCHAR(512),
        amount VARCHAR(512),
        result VARCHAR(512),
        PRIMARY KEY (subNumber, department)
        )
        DEFAULT CHARSET utf8
      """.stripMargin
    stmt.executeUpdate(qTableBjCont)

    // ZB tables
    val qTableZb =
      s"""CREATE TABLE IF NOT EXISTS ${TB_ZB} (
        searchKey VARCHAR(255),
        name VARCHAR(512),
        date VARCHAR(512),
        category VARCHAR(512),
        subNumber VARCHAR(255),
        segmentNumber VARCHAR(512),
        caller VARCHAR(512),
        callerAgency VARCHAR(512),
        contact VARCHAR(512),
        contactTel VARCHAR(512),
        contactAddr VARCHAR(512),
        contactPost VARCHAR(512),
        subType VARCHAR(512),
        segmentName VARCHAR(512),
        url VARCHAR(512),
        PRIMARY KEY (subNumber, searchKey)
        )
        DEFAULT CHARSET utf8
      """.stripMargin
    stmt.executeUpdate(qTableZb)

    val qTableZbCand =
      s"""CREATE TABLE IF NOT EXISTS ${TB_ZB_CAND} (
        subNumber VARCHAR(255),
        candOrdInt VARCHAR(128),
        agency VARCHAR(512),
        candOrd VARCHAR(512),
        manager VARCHAR(512),
        price VARCHAR(512),
        PRIMARY KEY (subNumber, candOrdInt)
        )
        DEFAULT CHARSET utf8
      """.stripMargin
    stmt.executeUpdate(qTableZbCand)

    // Dim table
    stmt.executeUpdate(s"CREATE TABLE IF NOT EXISTS ${TB_JSDW} (name VARCHAR(512)) DEFAULT CHARSET utf8")
    stmt.executeUpdate(s"CREATE TABLE IF NOT EXISTS ${TB_JLDW}  (name VARCHAR(512)) DEFAULT CHARSET utf8")
    stmt.executeUpdate(s"CREATE TABLE IF NOT EXISTS ${TB_BJ_SUCC} (name VARCHAR(512)) DEFAULT CHARSET utf8")
    stmt.executeUpdate(s"CREATE TABLE IF NOT EXISTS ${TB_ZB_SUCC}  (name VARCHAR(512)) DEFAULT CHARSET utf8")
  }

  private def createTablesSqlServer(stmt: Statement): Unit = {
    def executeQuery(q: String) = {
      logger.debug(q)
      stmt.executeUpdate(q)
    }

    // Create backend database
    executeQuery(s"if db_id('${DB_NAME}') is null CREATE DATABASE ${DB_NAME}")

    try {
      executeQuery(s"CREATE SCHEMA ${DB_NAME}")
    } catch {
      case e: SQLServerException =>
    }

    // HP
    executeQuery(
      s"""if object_id('${TB_HP}', 'U') is null
          CREATE TABLE ${TB_HP} (
            projectName VARCHAR(512) PRIMARY KEY,
            constLoc VARCHAR(512),
            const VARCHAR(512),
            ecoEvalDepart VARCHAR(512),
            confirmFileName VARCHAR(512),
            confirmFileNo VARCHAR(512),
            confirmTime VARCHAR(512),
            confirmFileLink VARCHAR(512),
            postTime VARCHAR(512)
          )
      """.stripMargin
    )

    // BJ
    executeQuery(
      s"""if object_id('${TB_BJ}', 'U') is null
          CREATE TABLE ${TB_BJ} (
            searchKey VARCHAR(512),
            subNumber VARCHAR(512) PRIMARY KEY,
            projectName VARCHAR(512),
            subTime VARCHAR(512),
            subCategory VARCHAR(512),
            permission VARCHAR(512),
            const VARCHAR(512),
            constLoc VARCHAR(512),
            investment VARCHAR(512),
            constScale VARCHAR(512),
            agency VARCHAR(512),
            url VARCHAR(512)
            )
      """.stripMargin
    )

    executeQuery(
      s"""if object_id('${TB_BJ_CTC}', 'U') is null
          CREATE TABLE ${TB_BJ_CTC} (
            subNumber VARCHAR(512),
            name VARCHAR(512),
            department VARCHAR(512),
            manager VARCHAR(512),
            category VARCHAR(512),
            amount VARCHAR(512),
            result VARCHAR(512),
            PRIMARY KEY (subNumber, department)
            )
      """.stripMargin
    )

    // ZB
    executeQuery(
      s"""if object_id('${TB_ZB}', 'U') is null
          CREATE TABLE ${TB_ZB} (
            searchKey VARCHAR(512),
            name VARCHAR(512),
            date VARCHAR(512),
            category VARCHAR(512),
            subNumber VARCHAR(512),
            segmentNumber VARCHAR(512),
            caller VARCHAR(512),
            callerAgency VARCHAR(512),
            contact VARCHAR(512),
            contactTel VARCHAR(512),
            contactAddr VARCHAR(512),
            contactPost VARCHAR(512),
            subType VARCHAR(512),
            segmentName VARCHAR(512),
            url VARCHAR(512),
            PRIMARY KEY (subNumber, searchKey)
            )
      """.stripMargin
    )

    executeQuery(
      s"""if object_id('${TB_ZB_CAND}', 'U') is null
          CREATE TABLE ${TB_ZB_CAND} (
            subNumber VARCHAR(512),
            candOrdInt VARCHAR(128),
            agency VARCHAR(512),
            candOrd VARCHAR(512),
            manager VARCHAR(512),
            price VARCHAR(512),
            PRIMARY KEY (subNumber, candOrdInt)
            )
      """.stripMargin
    )

    // Dim table
    executeQuery(s"if object_id('${TB_JSDW}', 'U') is null CREATE TABLE ${TB_JSDW} (name VARCHAR(512))")
    executeQuery(s"if object_id('${TB_JLDW}', 'U') is null CREATE TABLE ${TB_JLDW} (name VARCHAR(512))")
    executeQuery(s"if object_id('${TB_BJ_SUCC}', 'U') is null CREATE TABLE ${TB_BJ_SUCC} (name VARCHAR(512))")
    executeQuery(s"if object_id('${TB_ZB_SUCC}', 'U') is null CREATE TABLE ${TB_ZB_SUCC} (name VARCHAR(512))")
  }

  def executeUpsert(stmt: Statement, cmd: String): Unit = {
    if (isMysqlBackend) {
      stmt.executeUpdate(cmd)
    } else { // SQLServer
      try {
        stmt.executeUpdate(cmd)
      } catch {
        case e: SQLServerException =>
          if (!e.toString.contains("Violation of PRIMARY KEY"))
            throw e
        case e: Exception => throw e
      }
    }
  }

  def executeUpsertSilently(stmt: Statement, cmd: String) = {
    try {
      executeUpsert(stmt, cmd)
    } catch {
      case e: Exception =>
        logger.warn((e.getMessage :: Nil).mkString("\n"))
    }
  }

  // Generate history interval (startDay, endDay) according to current time
  def historyInterval(interval: Int): (String, String) = {
    val sdf = new SimpleDateFormat("yyyy-MM-dd")
    val cl = Calendar.getInstance()
    cl.setTimeInMillis(System.currentTimeMillis())
    val end = sdf.format(cl.getTime)

    val cl2 = Calendar.getInstance()
    cl2.setTimeInMillis(cl.getTimeInMillis)
    cl2.add(Calendar.DATE, -1 * interval)
    val start = sdf.format(cl2.getTime)
    (start, end)
  }

  // Unittest
  def main(args: Array[String]): Unit = {
    val (start, end) = historyInterval(HISTORY_DAYS)
    println(start)
    println(end)
  }
}
